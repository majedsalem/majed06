"""main"""
from fastapi import Depends, FastAPI, File

from requests import Session

from fastapi.openapi.utils import get_openapi

from .database import SessionLocal, engine

from . import crud, models, schemas

import os

import dropbox

app = FastAPI(title="Imagegram")
models.Base.metadata.create_all(bind=engine)

app = FastAPI()

# DropBox Access Token
dbx = dropbox.Dropbox(
    'sl.BGkgi0zV5Msk25MgTqSx0t5MuEO97N4wUF0Po_QOb8yNBa_piBc0C5yrJ0bCWpyEQ7lqpO6Q8A5pSkPPLzmjMGxOh60JYbkqFZQ1o23Av3BS8RXvfd4_nlPM9JN6EsuzVZomvYwdlmY1')


# Create swagger
def my_schema():
    """Create swagger."""
    openapi_schema = get_openapi(
        title="Imagegram",
        version="1.0",
        description="share us your activities",
        routes=app.routes,
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = my_schema


# Create connection to DB
def get_db():
    """Create connection to DB."""
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


# upload image to DropBox and save metadata to DB
@app.post('/upload_image/')
def upload_image_and_meta_data(user_id: int, image_data: schemas.ImageMetaDataCreate = Depends(),
                               img_file: bytes = File(...), db: Session = Depends(get_db)):
    """upload image to DropBox. takes request as file upload and string"""
    try:
        # Create dropbox path
        dest_path = os.path.join('/app', image_data.image_name)
        # Upload file as bytes to the path
        dbx.files_upload(img_file,
                         dest_path)
        # save metadata and userid to DB
        crud.create_user_image_meta_data(db=db, image_data=image_data, user_id=user_id)

        return {"success": True}

    except Exception as e:
        print('ERROR:', str(e))
        return {"success": False}


# TODO get_images_by_user
@app.post('/images_user/')
def get_images_by_user():
    """TODO get_images_by_user (List)"""
    return {"success": True}


# TODO get_images
@app.post('/images/')
def get_images():
    """TODO get_images (List)"""
    return {"success": True}


# TODO delete image by name
@app.delete('/image/')
def delete_image_by_name():
    """TODO delete image by name"""
    return {"success": True}
