"""CRUD"""
from sqlalchemy.orm import Session

from . import models, schemas


def create_user_image_meta_data(db: Session, image_data: schemas.ImageMetaDataCreate, user_id: int):
    """Create image metadata and user id."""
    db_image_data = models.ImageMetaData(**image_data.dict(), owner_id=user_id)
    db.add(db_image_data)
    db.commit()
    db.refresh(db_image_data)

    return db_image_data
# def get_user(db: Session, user_id: int):
#     return db.query(models.User).filter(models.User.id == user_id).first()
#
#
# def get_user_by_email(db: Session, email: str):
#     return db.query(models.User).filter(models.User.email == email).first()
#
#
# def get_users(db: Session, skip: int = 0, limit: int = 100):
#     return db.query(models.User).offset(skip).limit(limit).all()
